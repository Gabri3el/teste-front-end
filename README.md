<h1 align="center">Organizador de Publicações | VueJS </h1>
<p align="center">Teste Front-End</p>
<img src="https://s6.gifyu.com/images/imagea7354e8aa98c5183.png">
<h1 align="center">
    <a href="https://cli.vuejs.org/">🔗  VueJs</a>
</h1>
<p align="center">🚀 Teste Front-End</p>


<p align="center">
 <a href="#Objetivo">Objetivo</a> • 
 <a href="#Install">Install instructions</a> • 
 <a href="#tecnologias">Tecnologias</a> • 
 <a href="#autor">Autor</a>• 
</p>

## Objetivo?

O objetivo deste desafio é avaliar minhas  habilidades como candidato à vaga de Front-end. 


## Descrição do projeto 

O desafio consiste em implementar uma aplicação client-side. Seguindo o arquivo de [história](HISTORY.md) 
Desenvolvimento conforme os requisitos e específicação de design detalhadas no Figma.

Para popular a tela foi usado um MOCKDATA `mock_data.json` na pasta `src/assets/` 
Os campos desse objeto JSON são mapeados da seguinte:

![cardexemplosofrimento](/imgs/card_exemplo_sofrimento.png)


## Install instructions

### Getting Started

#### 1) Clone & Install Dependencies on Linux/MacOS

- 1.1) `git clone `
- 1.2) `cd TESTE-FRONT-END` - cd para entrar na pasta criada.
- 1.3)  Instale Pacotes NPM com `yarn install`

#### 2) Clone & Install Dependencies on Windows

- 2.1) `git clone`
- 2.2)  Abra o Windows Powershell como Administrador.
- 2.3) `cd TESTE-FRONT-END` - cd para entrar na pasta criada.
- 2.4)  Instale Pacotes com `yarn install` ou `npm install`

#### 3) Start your app

- 3.1) $ npm run serve




## :zap: Tecnologias

<h1 align="center">
  <img src="https://cli.vuejs.org/favicon.png" alt="Stack" height="150" width="600">
  <br>
</h1>

-   [VueJs](https://github.com/vuejs/vue)
-   [Element-Plus](https://github.com/element-plus/element-plus)
-   [Saas](https://www.npmjs.com/package/saas)
-   [Metodologia BEM]
-   [moment](https://github.com/moment/moment)


### Autor
---
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/62441006?v=4" width="100px;" alt=""/>
 <br />
 <sub><b>Gabriel Merino</b></sub></a> <a href="https://github.com/Gabri3el/" title="github">🚀</a>

 Feito com maior dedicação por Gabriel Merino!

[![Linkedin Badge](https://img.shields.io/badge/-Gabriel-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/gabrielmerinostos/)](https://www.linkedin.com/in/gabrielmerinostos/)
[![Gmail Badge](https://img.shields.io/badge/-gabrielmerino.dev@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:gabrielmerino.dev@gmail.com)](mailto:gabrielmerino.dev@gmail.com)



```
